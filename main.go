package main

import (
	//"bufio"
	"bufio"
	"bytes"
	"encoding/csv"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"math"
	"net/http"
	"os"
	"os/exec"
	"runtime"
	"strconv"
	"strings"
	"time"

	"gitlab.com/controllercubiomes/util"
)

//const url = "http://controller.sevisapickyasshole.net"
const url = "https://seedhunt.net/controller"
const clientName = "analyzer"

func toFloat(s string) (f float32) {
	t, err := strconv.ParseFloat(s, 32)
	if err != nil {
		log.Println(err)
	}
	f = float32(t)
	return
}

func toInt64(s string) (i int64) {
	i, _ = strconv.ParseInt(s, 10, 64)
	return
}

func getChunk(client *http.Client, key string) (chunk util.AnalyzeChunk) {
	got := false
	for !got {
		req, err := http.NewRequest("GET", url+"/analyzechunk", nil)
		if err != nil {
			log.Println(err)
			continue
		}
		req.Header.Set("authorization", key)
		resp, err := client.Do(req)
		if err != nil {
			log.Println(err)
			continue
		}
		defer resp.Body.Close()
		if resp.StatusCode != 200 {
			log.Printf("Error from server, is your criteria valid? errorcode: %d", resp.StatusCode) //TODO: actually handle
			log.Println("HITT")
			continue
		}

		err = json.NewDecoder(resp.Body).Decode(&chunk)
		if err != nil {
			log.Println(err)
			continue
		}
		got = true
	}
	return
}

var key string

func main() {
	var threads int
	var downloadSearcher bool
	flag.StringVar(&key, "k", "", "Key to analyze with") //TODO: remove thest and add back user interface for choosing criteria
	flag.IntVar(&threads, "t", runtime.NumCPU(), "Number of threads you want to use. You have "+string(runtime.NumCPU())+" threads available")
	flag.BoolVar(&downloadSearcher, "d", true, "Download searcher automatically")
	flag.Parse()
	if len(os.Args) > 1 && key == "" {
		fmt.Println("When using args you need to also give a key with the arg -k")
		fmt.Println("For a full list of args run the program with arg --help")
		fmt.Println("You can also run the program without any arguments")
		return
	} else if len(os.Args) == 1 {
		reader := bufio.NewReader(os.Stdin)
		for key == "" {
			fmt.Print("Enter key you were given: ")
			key, _ = reader.ReadString('\n')
			key = strings.TrimSpace(key)
		}
		fmt.Printf("Enter the amount of threads you want to use. You have (%d) available: ", runtime.NumCPU())
		threadsString, _ := reader.ReadString('\n')
		var err error
		threads, err = strconv.Atoi(strings.TrimSpace(threadsString))
		if err != nil {
			log.Fatal(err)
		}
	}
	client := &http.Client{}
	chunk := getChunk(client, key)
	log.Println(chunk.Seeds)
	if downloadSearcher {
		if err := DownloadFile("./"+clientName, chunk.ProgramURL); err != nil {
			log.Fatal(err)
		}
	}
	for {
		lastSeed := int64(0)
		log.Println("START!!!!!!")
		for _, seed := range chunk.Seeds {
			if seed == lastSeed {
				continue
			}
			var cmd *exec.Cmd
			if runtime.GOOS == "windows" {
				cmd = exec.Command(clientName+".exe", strconv.FormatInt(seed, 10), strconv.Itoa(chunk.RadiusInner), strconv.Itoa(chunk.RadiusOuter))
			} else {
				cmd = exec.Command("./"+clientName, strconv.FormatInt(seed, 10), strconv.Itoa(chunk.RadiusInner), strconv.Itoa(chunk.RadiusOuter))
			}
			stdout, err := cmd.StdoutPipe()
			if err != nil {
				log.Println("Setting up pie: ", err)
				continue
			}

			err = cmd.Start()
			if err != nil {
				log.Println("Start: ", err)
				continue
			}
			log.Println("Started")

			go readStdout(stdout)

			err = cmd.Wait()
			if err != nil {
				log.Println(err)
			}
			lastSeed = seed
		}

		log.Printf("Getting a new cunk. Had this, %v", chunk)
		time.Sleep(1 * time.Second) // Might be enugh to sync the last analyzed object
		chunk = getChunk(client, key)
		log.Printf("Got this new chunk, %v", chunk)
		backoff := float64(2)
		for len(chunk.Seeds) == 0 {
			log.Println("Sleeping abit since there is no work for me")
			time.Sleep(time.Duration(math.Pow(2, backoff)) * time.Second)
			if backoff < 12 { //Magic cutoff number for 4k secounds
				backoff++
			}
			log.Println("Just waking up to check if there is something for me to do")
			chunk = getChunk(client, key)
		}
		log.Println("END!!!!!!")
	}
}

func readStdout(stdout io.ReadCloser) {
	defer stdout.Close()
	for {
		line, err := csv.NewReader(stdout).Read()
		if err != nil {
			log.Println(err)
			break
			//continue
		}
		if len(line) != 84 {
			log.Printf("The output only gave %d elements.\n%v\n", len(line), line)
			continue
		}
		seed := util.Seed{
			Seed:             toInt64(line[0]),
			Spawn_biome:      line[1],
			Village_distance: int(toInt64(line[2])),
			Huts:             int(toInt64(line[3])),
			Monuments:        int(toInt64(line[4])),
			Ocean: util.Ocean{
				Cold_ocean:          toFloat(line[13]),
				Deep_cold_ocean:     toFloat(line[17]),
				Deep_frozen_ocean:   toFloat(line[18]),
				Deep_lukewarm_ocean: toFloat(line[19]),
				Deep_ocean:          toFloat(line[20]),
				Deep_warm_ocean:     toFloat(line[21]),
				Frozen_ocean:        toFloat(line[31]),
				Frozen_river:        toFloat(line[32]),
				Lukewarm_ocean:      toFloat(line[42]),
				Ocean:               toFloat(line[53]),
				Warm_ocean:          toFloat(line[79]),
			},

			Mesa: util.Mesa{
				Badlands:                         toFloat(line[5]),
				Badlands_plateau:                 toFloat(line[6]),
				Eroded_badlands:                  toFloat(line[28]),
				Modified_badlands_plateau:        toFloat(line[43]),
				Modified_wooded_badlands_plateau: toFloat(line[47]),
				Wooded_badlands_plateau:          toFloat(line[81]),
			},

			Desert: util.Desert{
				Desert:       toFloat(line[22]),
				Desert_hills: toFloat(line[23]),
				Desert_lakes: toFloat(line[24]),
			},

			Savanna: util.Savanna{
				Savanna:                   toFloat(line[56]),
				Savanna_plateau:           toFloat(line[57]),
				Shattered_savanna:         toFloat(line[58]),
				Shattered_savanna_plateau: toFloat(line[59]),
			},

			Plains: util.Plains{
				Plains:           toFloat(line[54]),
				Sunflower_plains: toFloat(line[69]),
			},

			Swamp: util.Swamp{
				Swamp:       toFloat(line[70]),
				Swamp_hills: toFloat(line[71]),
			},

			Forest: util.Forest{
				Birch_forest:       toFloat(line[11]),
				Birch_forest_hills: toFloat(line[12]),
				Flower_forest:      toFloat(line[29]),
				Forest:             toFloat(line[30]),
				Tall_birch_forest:  toFloat(line[75]),
				Tall_birch_hills:   toFloat(line[76]),
				Wooded_hills:       toFloat(line[82]),
			},

			Roofed: util.Roofed{
				Dark_forest:       toFloat(line[15]),
				Dark_forest_hills: toFloat(line[16]),
			},

			Mountain: util.Mountain{
				Gravelly_mountains:          toFloat(line[37]),
				Modified_gravelly_mountains: toFloat(line[44]),
				Mountain_edge:               toFloat(line[48]),
				Mountains:                   toFloat(line[49]),
				Wooded_mountains:            toFloat(line[83]),
			},

			Mushroom: util.Mushroom{
				Mushroom_fields:      toFloat(line[50]),
				Mushroom_field_shore: toFloat(line[51]),
			},

			Jungle: util.Jungle{
				Bamboo_jungle:        toFloat(line[7]),
				Bamboo_jungle_hills:  toFloat(line[8]),
				Jungle:               toFloat(line[39]),
				Jungle_edge:          toFloat(line[40]),
				Jungle_hills:         toFloat(line[41]),
				Modified_jungle:      toFloat(line[45]),
				Modified_jungle_edge: toFloat(line[46]),
			},

			Taiga: util.Taiga{
				Snowy_taiga:           toFloat(line[63]),
				Snowy_taiga_hills:     toFloat(line[64]),
				Snowy_taiga_mountains: toFloat(line[65]),
				Taiga:                 toFloat(line[72]),
				Taiga_hills:           toFloat(line[73]),
				Taiga_mountains:       toFloat(line[74]),
			},

			MegaTaiga: util.MegaTaiga{
				Giant_spruce_taiga:       toFloat(line[33]),
				Giant_spruce_taiga_hills: toFloat(line[34]),
				Giant_tree_taiga:         toFloat(line[35]),
				Giant_tree_taiga_hills:   toFloat(line[36]),
			},

			Snowy: util.Snowy{
				Ice_spikes:            toFloat(line[38]),
				Snowy_beach:           toFloat(line[61]),
				Snowy_mountains:       toFloat(line[62]),
				Snowy_taiga:           toFloat(line[63]),
				Snowy_taiga_hills:     toFloat(line[64]),
				Snowy_taiga_mountains: toFloat(line[65]),
				Snowy_tundra:          toFloat(line[66]),
			},
			Basalt_deltas:     toFloat(line[9]),
			Beach:             toFloat(line[10]),
			Crimson_forest:    toFloat(line[14]),
			End_barrens:       toFloat(line[25]),
			End_highlands:     toFloat(line[26]),
			End_midlands:      toFloat(line[27]),
			Nether_wastes:     toFloat(line[52]),
			River:             toFloat(line[55]),
			Small_end_islands: toFloat(line[60]),
			Soul_sand_valley:  toFloat(line[67]),
			Stone_shore:       toFloat(line[68]),
			The_end:           toFloat(line[77]),
			The_void:          toFloat(line[78]),
			Warped_forest:     toFloat(line[80]),
		}
		err = sendSeed(seed)
		if err != nil {
			log.Println(err)
			continue
		}
	}
}

func DownloadFile(filepath string, url string) error {
	// Get the data
	if runtime.GOOS == "windows" {
		url = url + ":windows"
		url = strings.ReplaceAll(url, "<file>", filepath[2:])
		filepath = filepath[2:] + ".exe"
	} else {
		url = url + ":linux"
		url = strings.ReplaceAll(url, "<file>", filepath[2:])
	}
	log.Println("Temp log for file download:\n", url)
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Create the file
	out, err := os.OpenFile(filepath, os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		return err
	}
	defer out.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	return err
}

func sendSeed(seed util.Seed) (err error) {
	data, err := json.Marshal(seed)
	if err != nil {
		log.Println(err)
		return
	}
	log.Println(seed)
	sent := false
	var req *http.Request
	for !sent {
		req, err = http.NewRequest("POST", url+"/analyzedseed", bytes.NewBuffer(data))
		if err != nil {
			log.Println(err)
			return
		}
		req.Header.Set("authorization", key)
		client := &http.Client{}
		_, err = client.Do(req)
		if err != nil {
			log.Println(err)
			return
		}
		sent = true
	}
	return
}
